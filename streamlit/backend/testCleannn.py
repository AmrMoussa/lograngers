import re
import nltk

# Download the words corpus (if not already downloaded)


def clean_sentences(sentences):
    cleaned_sentences = []
    words = set(nltk.corpus.words.words())

    for sentence in sentences:
        # Use regular expression to remove timestamp, hostname, ID, etc.
        
        # Join the English words back into a sentence
        cleaned_sentence = " ".join(w for w in nltk.wordpunct_tokenize(sentence) \
         if w.lower() in words or not w.isalpha())

        cleaned_sentences.append(cleaned_sentence)

    return cleaned_sentences

# Example usage
log_lines = [
    "Nov 09 13:11:39 CMX50070-101776 nginx[885]: nginx: configuration file /etc/nginx/nginx.conf test is successful",
    "Nov 09 13:11:39 CMX50070-101776 systemd[1]: Started The NGINX HTTP and reverse proxy server.",
    "Nov 09 13:11:39 CMX50070-101776 NetworkManager[432]: <info>  [1699535499.3372] hostname: hostname: using hostnamed"
]

cleaned_logs = clean_sentences(log_lines)

# Print the cleaned logs
for log in cleaned_logs:
    print(log)
