# this file is responsible for gettting a text file, vectorization ... 
import os
from transformers import BertTokenizer, BertModel
import torch
import re
import nltk
import concurrent.futures


nltk.download('words')


class LogServer:

    def __init__(self, model_name='bert-large-uncased-whole-word-masking-finetuned-squad'):
        self.tokenizer = BertTokenizer.from_pretrained(model_name)
        self.model = BertModel.from_pretrained(model_name)


    def uploadFile(self, file_path):
        with open(file_path, 'r') as file:
            lines = file.readlines()

        # create a vector representatoin for every line.
        print(len(lines))
        # lines = lines[slice(1000)]
        # print(lines)
        cleaned_logs = self.clean_sentences(lines)
        print(f"cleaned : {len(cleaned_logs)}")

        vectorRepresentation = self.embed_log(cleaned_logs)
        print(f"final: {len(vectorRepresentation)}")
        return vectorRepresentation
    

   
    def embed_log(self, logs, batch_size=1000):
        print("started")
        embeddings = []
        total_logs = len(logs)

        def process_batch(start_index):
            end_index = min(start_index + batch_size, total_logs)
            batch_logs = logs[start_index:end_index]
            return self.embed(batch_logs)

        with concurrent.futures.ThreadPoolExecutor() as executor:
            # Use executor to process batches in parallel
            futures = [executor.submit(process_batch, i) for i in range(0, total_logs, batch_size)]

            # Collect results as they become available
            for future in concurrent.futures.as_completed(futures):
                try:
                    result = future.result()
                    embeddings.extend(result)
                except Exception as e:
                    print(f"Error processing batch: {e}")

        return embeddings



    def embed(self, sentences):
        # Tokenize and convert to tensor
        print(len(sentences))
        tokens = self.tokenizer(sentences, return_tensors='pt', padding=True, truncation=True)
        print("tokens in")
        # Forward pass through the model
        with torch.no_grad():
            print("tokens in23")
            outputs = self.model(**tokens)
        print("tokens out")
        # Extract the embeddings (last layer hidden states)
        last_hidden_states = outputs.last_hidden_state

        # Use the embeddings of the [CLS] token as representations for the entire sentence
        sentence_embeddings = last_hidden_states[:, 0, :].numpy()

        return sentence_embeddings
    
    def clean_sentences(self,sentences):
        cleaned_sentences = []
        english_words = set(nltk.corpus.words.words())

        for sentence in sentences:
            # Use regular expression to remove timestamp, hostname, ID, etc.
            cleaned_sentence = re.sub(r'\b(?:\w{3} \d{2} \d{2}:\d{2}:\d{2} \w+|\w+:\[\d+\]|\w+-\d+-\d+)\b', '', sentence)

            cleaned_sentence = re.sub(r'[:.]+', '', cleaned_sentence)
            
            # Keep only English words
            english_tokens = [word for word in cleaned_sentence.split() if word.lower() in english_words and len(word) > 2]

            # Create a new sentence with only English words
            if len(english_tokens) > 1:
                cleaned_sentence = ' '.join(english_tokens)

                # Remove extra spaces
                cleaned_sentence = ' '.join(cleaned_sentence.split())
                
                cleaned_sentences.append(cleaned_sentence)

        return cleaned_sentences
    
    def calculate_similarity(question_vector, answer_vector):
        similarity = 1 - cosine(question_vector[0], answer_vector[0])
        return similarity
    
    
    
