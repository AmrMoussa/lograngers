import streamlit as st
import os


#def upload_file():
#st.title("Uploader")

uploaded_file = st.file_uploader("Log file", type=["txt"], accept_multiple_files=False)
#st.write("test")
if uploaded_file is not None:

    file_path = os.path.join("log_data", uploaded_file.name)

    with open(file_path, "wb") as f:
        f.write(uploaded_file.getvalue())
    st.success(f"The file is successfully uploaded")

    # Pass the file path to the backend (replace this with your backend logic)
    #st.write("Passing file path to backend:")


    #st.write(file_path)
    #return file_path
