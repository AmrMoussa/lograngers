# this file is responsible for gettting a text file, vectorization ... 

import os
from dotenv import load_dotenv
import openai

# key = os.getenv('OPENAI_KEY')


class LogServer:

    def __init__(self):
        load_dotenv()
        pass


    def uploadFile(self, file_path='/streamlit/log_data'):
        with open(file_path, 'r') as file:
            lines = file.readlines()

        # extract individual logs 
        lines = [line for line in lines]

        # create a vector representatoin for every line.
        vectorRepresentation = []
        for line in lines:
           vectorRepresentation.append(self.embed_log(line))

        return vectorRepresentation
    

   
    def embed_log(self,logs, batch_size=500):
        embeddings = []
        total_logs = len(logs)

        for start_index in range(0, total_logs, batch_size):
            end_index = min(start_index + batch_size, total_logs)
            batch_logs = logs[start_index:end_index]

        
            embedding_response = openai.Embedding.create(
                model="text-embedding-ada-002",
                inputs=batch_logs
            )

            batch_embeddings = [
                [start_index + index, item['embedding']]
                for index, item in enumerate(embedding_response['data'])
            ]
            print("emedding new batch")
            embeddings.extend(batch_embeddings)

        return embeddings

    def calculate_similarity(question_vector, answer_vector):
        similarity = 1 - cosine(question_vector[0], answer_vector[0])
        return similarity
    