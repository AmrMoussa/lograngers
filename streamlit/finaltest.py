import streamlit as st
import requests
from backend import LogServer
# Set the URL of your backend endpoint
backend_url = 'http://your-backend-url/upload'

def main():
    st.title("File Uploader and Backend Communicator")

    # File uploader
    uploaded_file = st.file_uploader("Upload a file")

    if uploaded_file is not None:
        # Display the content of the uploaded file
        st.text_area("Uploaded File Content:", uploaded_file.read(), height=200)

        # Button to trigger the backend communication
        if st.button("Send to Backend"):
            # Send the uploaded file to the backend
            backend_response = send_to_backend(uploaded_file)

            # Display the backend response
            st.text_area("Backend Response:", backend_response, height=100)

def send_to_backend(uploaded_file):
    try:
        # Prepare the file for the POST request
        files = {'file': ('uploaded_file.txt', uploaded_file)}

        # Make a POST request to the backend
        response = requests.post(backend_url, files=files)

        # Check if the request was successful
        if response.status_code == 200:
            return response.text
        else:
            return f"Error: {response.status_code} - {response.text}"

    except Exception as e:
        return f"Error: {e}"

if __name__ == "__main__":
    main()
