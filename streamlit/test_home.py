import streamlit as st
import openai
import os
#from "../backend/server" import LogServer
# Set your OpenAI API key
openai.api_key = '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'

def generate_response(prompt):
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=150
    )
    return response.choices[0].text.strip()


# Function to handle file upload and chatbot interaction
def process_upload_and_chatbot(uploaded_file):
    # Check if a file was uploaded
    if uploaded_file is not None:

        file_path = os.path.join("log_data", uploaded_file.name)

        with open(file_path, "wb") as f:
            f.write(uploaded_file.getvalue())
        st.success(f"The file is successfully uploaded")


# Function to highlight important lines in a file
def highlight_lines(file_content, keywords):
    highlighted_lines = []

    #for line in file_content.split('\n'):
    for line in file_content:
        #line=str(line,encoding='utf-8')
        # Check if any of the keywords are present in the line
        if any(keyword.lower() in str(line).lower() for keyword in keywords):
            line = f'<span style="color:red; font-weight:bold;">{str(line)}</span>'
        highlighted_lines.append(line)
  #      st.write(1)

    return '\n'.join(highlighted_lines)


def main():
  


    st.title("OpenAI Chatbot")


    #File Uploader
    col1, col2 = st.columns([0.5, 0.5])

    # Smaller file uploader button
    with col1:
        uploaded_file = st.file_uploader("", type=["txt"], key="small_uploader")

    # Process upload and interact with the chatbot
    with col2:
        process_upload_and_chatbot(uploaded_file)

    # Get user input
    user_input = st.text_input("You:", "")


    if user_input:
        # Generate a response using OpenAI API
        bot_response = generate_response(user_input)

        # Display the chat
        st.text_area("Bot:", bot_response, height=100)


    keywords = ["error", "warning", "interept","missed","not found"]

        # Process upload and highlight important lines
    if uploaded_file is not None:
        file_contents = uploaded_file.read()
        st.text_area("Original File Contents", file_contents, height=200)
        #st.write("Original File Contents:")
        st.code(file_contents)

        # Highlight important lines
        highlighted_contents = highlight_lines(file_contents, keywords)
        st.text_area("Highlighted File Contents", highlighted_contents, height=200)
        #st.write("Highlighted File Contents:")
        #st.code(highlighted_contents)

if __name__ == "__main__":
    main()