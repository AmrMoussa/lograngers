import streamlit as st
import openai
import os
from backend import LogServer
#from "../backend/server" import LogServer
# Set your OpenAI API key
openai.api_key = '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'

def generate_response(prompt):
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=150
    )
    return response.choices[0].text.strip()

# Function to handle file upload and chatbot interaction
def process_upload_and_chatbot(uploaded_file):
    # Check if a file was uploaded
    if uploaded_file is not None:

        file_path = os.path.join("log_data", uploaded_file.name)

        with open(file_path, "wb") as f:
            f.write(uploaded_file.getvalue())
        st.success(f"The file is successfully uploaded")


def main():


    st.title("Log Ranger To Rescue")

    #File Uploader
    col1, col2 = st.columns([1, 1])

    # Smaller file uploader button
    with col1:
        uploaded_file = st.file_uploader("", type=["txt"], key="small_uploader", help="")

    # Process upload and interact with the chatbot
    with col2:
        process_upload_and_chatbot(uploaded_file)

    # Get user input
    user_input = st.text_input("Technician:", "")

    if user_input:
        # Generate a response using OpenAI API
        bot_response = generate_response(user_input)

        # Display the chat
        st.text_area("LogRanger Expert", bot_response, height=100)

if __name__ == "__main__":
    main()